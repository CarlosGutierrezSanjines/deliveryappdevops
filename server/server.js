var http = require('http');
var express = require('express');
var fs = require('fs');
var app = express();
var bodyParser = require("body-parser");

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.urlencoded({extended: true}))

let products = "http://delivapi:8000/api/products";
//let products = "http://localhost:8000/api/products";

const port = 8001;
//const port = 8010;

app.get('/', function (req, res) {
  res.render('home');
});

app.get('/products', function(req, res){
  console.log("Show Products");
  http.get(products,(response) => {
    let body = "";
    response.on("data", (chunk) => {
      body += chunk;
    });

    response.on("end", () => {
      try {
        let prods = JSON.parse(body);
        console.log(prods);
        res.render('products', {prods:prods});
      } catch (error) {
        console.error(error.message);
      };
    });

    }).on("error", (error) => {
      res.render('home');
      console.error(error.message);
  });
})

app.post('/orders', function(req, res) {
  //console.log(req.body);
  const order = JSON.parse(JSON.stringify(req.body));
  console.log(order);
  res.render('orders');
})

app.get('/orders', function(req, res) {
  res.render('orders');
})

var server = app.listen(port, function () {
  console.log('Node server is running..');
});