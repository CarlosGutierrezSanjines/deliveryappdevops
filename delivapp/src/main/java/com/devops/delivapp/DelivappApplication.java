package com.devops.delivapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelivappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DelivappApplication.class, args);
	}

}
