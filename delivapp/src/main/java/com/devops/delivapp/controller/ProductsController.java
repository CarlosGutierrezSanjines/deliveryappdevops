package com.devops.delivapp.controller;

import com.devops.delivapp.model.Product;
import com.devops.delivapp.repository.IProductsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/api")
public class ProductsController {

  @Autowired
  private IProductsRepository repository;

  @GetMapping(path="/products")
  public @ResponseBody Iterable<Product> getAllProducts() {
    return repository.findAll();      
  }
}