mysql {
  dbuser = 'intern'
  dbpass = 'pass'
}

environments {
  dev {
    host = 'localhost:3306'
  }

  prod {
    host = 'delivdb'
  }

  qa {
    host = 'delivdb'
  }
}