CREATE TABLE products (
  id SMALLINT NOT NULL AUTO_INCREMENT,
  p_name varchar(50) NOT NULL,
  price INT NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE clients(
  id INT NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  addrs varchar(50) NOT NULL,
  pass varchar(50) NOT NULL,
  PRIMARY KEY(id)
);
CREATE TABLE orders(
  id INT NOT NULL AUTO_INCREMENT,
  client INT NOT NULL REFERENCES clients(id),
  product INT NOT NULL REFERENCES products(id),
  quantity INT NOT NULL,
  PRIMARY KEY(id)
);