node{

  parameters {
    choice(choices: "qa\ndev", description: 'environment type', name: 'ENVIRONMENT')
    string(defaultValue: '', description: 'Ip of the machine to deploy images', name: 'IP')
  }
  def remote = [:]
  remote.name = 'VagrantVM'
  remote.host = "${IP}"
  remote.user = 'vagrant'
  remote.password = 'vagrant'
  remote.allowAnyHosts = true
  apiregistry = "carlosgutierrezsanjines/javaapi"
  registryCredential = 'dockerhubCarlos'
  apiImage = ''

  stage ("Clone") {
    git 'https://gitlab.com/CarlosGutierrezSanjines/deliveryappdevops.git'
  }
  stage ("Build Artifact") {
    parallel (
      Build: { dir('delivapp'){bat "gradlew clean build -Penv=${params.ENVIRONMENT}"} },
      CleanImages: { sshCommand remote: remote, command: "docker image prune -af" }
    )
  }
  stage ("Build Images") {
    parallel (
      Build: {
        dir('delivapp'){
          script {
              apiImage = docker.build apiregistry + ":$BUILD_NUMBER"
          }
        }
      },
      CreateENV: {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          bat "rm .env"
        }
        bat "echo ROOT_PASSWORD=carlos>>.env"
        bat "echo MYUSER=intern>>.env"
        bat "echo MYPASSWORD=pass>>.env"
        bat "echo DB_ROUTE=.>>.env"
        bat "echo APPVERSION=$BUILD_NUMBER>>.env"
      }
    )

  }
  stage("Image Publishing"){
    parallel(
      Publish:{
        script {
          docker.withRegistry( '', registryCredential ) {
            apiImage.push()
        }
      }
      },
      Export: {
        echo "Infraestructure for ${params.ENVIRONMENT}"
        sshPut remote: remote, from: '.env', into: '.'
        sshPut remote: remote, from: './mysqlDB/', into: '.'
        sshPut remote: remote, from: './server/', into: '.'
        if ("${params.ENVIRONMENT}" == 'prod') {
            sshPut remote: remote, from: 'docker-compose.yml', into: '.'
        } else {
            sshPut remote: remote, from: './delivapp/docker-compose.yml', into: '.'
        }
      }
    )
  }
  stage('Remove Unused') {
    parallel(
      RemoveLocal: { bat "docker rmi $apiregistry:$BUILD_NUMBER" },
      RemoveRemote: {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker stop delivapi"
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker stop delivdb "
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker stop ${remote.user}_delivserver_1"
          sshCommand remote: remote, command: "docker stop ${remote.user}_delivserver_2"
          sshCommand remote: remote, command: "docker stop ${remote.user}_delivserver_3"
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker stop balancer"
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker rm delivapi"
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker rm ${remote.user}_delivserver_1"
          sshCommand remote: remote, command: "docker rm ${remote.user}_delivserver_2"
          sshCommand remote: remote, command: "docker rm ${remote.user}_delivserver_3"
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker rm delivdb"
        }
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sshCommand remote: remote, command: "docker rm balancer"
        }
      }
    )
  }
  stage("Pulling Images") {
    retry(count: 3){
      sshCommand remote: remote, command: "docker pull $apiregistry:$BUILD_NUMBER"
      sshCommand remote: remote, command: "docker images"
    }
  }
  stage ("Deploy Infraestructure") {
    sshCommand remote: remote, command: "docker-compose up -d"
    if("${params.ENVIRONMENT}" == 'prod') {
      stage("Scale APP"){
        sshCommand remote: remote, command: "docker-compose scale delivserver=3"
      }
    }
  }
  /* stage ("Remove Temporals") {
    sshCommand remote: remote, command: "rm .env"
    sshCommand remote: remote, command: "rm -r -f mysqlDB/"
  } */
}
